""" This is a simple module that represents common camera operations without ROS or other messy packages... Just matplotlib and numpy.

With This package, you can use the default camera (pointgrey gigE) and run simple tests of things, like translating points into camera frame and back into world frame given a known scale factor.

You can test your camera rotations. Our convention is R=RzRyRx. You can plot the relative orientation of your camera and its parent axes.

feed this module your rotations in degrees. remember as well that camera positive x points right, positive y down, and positive z into the image.


In all of the plotting functions included here, the arrows representing axes are red,blue,green for x,y,z respectively.

Alexander Brown and Bobby Leary, January 2014
"""
from numpy import *
from matplotlib.pyplot import *
from mpl_toolkits.mplot3d import Axes3D


class Camera():
    def __init__(self,K=array([[750.0, 0.000000, 320.0],[0.000000, 750.0, 240.0],[0.000000, 0.000000, 1.000000]]),roll=-90,pitch=90,yaw=0,Tx=0,Ty=0,Tz=-1.5):
        self.K=K
        self.roll=roll
        self.pitch=pitch
        self.yaw=yaw
        self.Tx=Tx
        self.Ty=Ty
        self.Tz=Tz

        self.createProjectionMatrix() #this defines P

    def plotAxes(self):
        """ This is designed to help you visualize how you've rotated the camera axes from the parent axes."""
        xax = array([1,0,0,1])
        yax = array([0,1,0,1])
        zax = array([0,0,1,1])
        origin = array([0,0,0,1])

        new_xax = dot(linalg.inv(self.E),xax)
        new_yax = dot(linalg.inv(self.E),yax)
        new_zax = dot(linalg.inv(self.E),zax)
        new_origin = dot(linalg.inv(self.E),origin)

        fig=figure()
        ax=fig.add_subplot(111,projection='3d')
        ax.plot([origin[0],xax[0]],[origin[1],xax[1]],[origin[2],xax[2]],'r',label='base x')
        ax.plot([origin[0],yax[0]],[origin[1],yax[1]],[origin[2],yax[2]],'g',label='base y')
        ax.plot([origin[0],zax[0]],[origin[1],zax[1]],[origin[2],zax[2]],'b',label='base z')
        # ax.annotate('vehicle',[0,0,0])
        
        ax.plot([new_origin[0],new_xax[0]],[new_origin[1],new_xax[1]],[new_origin[2],new_xax[2]],'r',label='base x')
        ax.plot([new_origin[0],new_yax[0]],[new_origin[1],new_yax[1]],[new_origin[2],new_yax[2]],'g',label='base y')
        ax.plot([new_origin[0],new_zax[0]],[new_origin[1],new_zax[1]],[new_origin[2],new_zax[2]],'b',label='base z')

        title('Base Axes and Camera Axes')
        xlabel('Base x (m)')
        ylabel('Base y (m)')
        axis('equal')

        show()

    def createProjectionMatrix(self):
        """ This defines the projection matrix given the input parameters through rotation, translation, and the intrinsic matrix. Distortion should be accounted for already TODO!!!"""

        # Create the translation vector
        # NOTE: This is the translation from camera to world, so if the camera is located
        # above the ground, then Tz is negative of that distance.
        # TODO: Do we just add the negatives here so the user just specifies positive values?
        self.T = array([[1,0,0,self.Tx],[0,1,0,self.Ty],[0,0,1,self.Tz],[0,0,0,1]])

        # Convert the angles from degrees to radians
        theta = radians(self.roll)
        alpha = radians(self.pitch) #90
        gamma = radians(self.yaw) #90

        # Create the rotation matrices for the X, Y, and Z rotations
        # We are rotating a point, so the negative sines are in the right location
        Rx = array([[1,0,0,0],[0,cos(theta),sin(theta),0],[0,-sin(theta),cos(theta),0],[0,0,0,1]])
        Ry = array([[cos(alpha),0,-sin(alpha),0],[0,1,0,0],[sin(alpha),0,cos(alpha),0],[0,0,0,1]])
        Rz = array([[cos(gamma),sin(gamma),0,0],[-sin(gamma),cos(gamma),0,0],[0,0,1,0],[0,0,0,1]])

        # Create our rotation matrix (Rz*Ry*Rx)
        self.R = dot(Rz,dot(Ry,Rx))

        # We are using homogeneous coordinates (4x4 in this case), so the extrinsic matrix is a rotation and translation
        # and it's simply just multiplying R*T.
        self.E = dot(self.R,self.T)

        # Let's make the intrinsic matrix homogeneous as well. (Just add a column and row of zeros and K(4,4) = 1)
        self.K = vstack([hstack([self.K, array([[0],[0],[0]])]),array([0,0,0,1])])

        # The projection matrix just becomes the multiplication of the intrinsic (K) and extrinsic (E) matrices.
        self.P = dot(self.K,self.E)

    def projectToImageRowCol(self,point):
        """ This takes a point in the camera's parent coordinate system and returns the *unscaled* top left corner-centered row and column that it corresponds to,
        as well as the scale factor w. It asks for a point as a 2-d numpy row vector aka array([[1,2,3]]). For true pixel values, u and v must be divided by w.
        """
        
        # Create homogeneous pixel coordinate by appending a 1 to make it 4x1.
        if len(point)<4:
           point=append(point,1)

        # Multiply by the projection matrix
        pixel = dot(self.P,transpose([point]))

        u = pixel[0]
        v = pixel[1]
        w = pixel[2]

        return u, v, w

    def projectToImageCentered(self,point):
        """ This takes a point in the camera's parent coordinate system and returns the image-centered row and column that it corresponds to.
        It asks for a point as a 2-d numpy row vector aka array([[1,2,3]]).
        """
        if len(point) < 4:
           point=append(point,1)

        # Multiply by the projection matrix
        pixel = dot(self.P,transpose([point]))

        # Divide by the last element in the output to get u,v pixel values
        x_i = int(pixel[0]/pixel[2]-self.K[0,2])
        y_i = int(pixel[1]/pixel[2]-self.K[1,2])

        return x_i, y_i

    def projectToFrame(self,pixel):

        # Create homogeneous pixel coordinate by appending a 1 to make it 4x1.
        if len(pixel) < 4:
           pixel=append(pixel,1)

        # Multiply by the projection matrix
        point = dot(linalg.inv(self.P),pixel)

        x = point[0]
        y = point[1]
        z = point[2]

        return x, y, z

if __name__ == '__main__':
    """ this is a demo for the camera module.  """
    print "You have chosen to run the demo."
    #first, we initialize a map instance. It will use the default filename in this case, and load the map.
    cam = Camera()
    #first let's just look at how the matrices behave.
    p = array([20,1,0])
    print "test point is x,y,z = " + str(p)
    u, v, w = cam.projectToImageRowCol(p)
    print "projected to image, this is col,row = " + str(int(u/w)) + "," + str(int(v/w))
    x,y,z = cam.projectToFrame(array([u,v,w]))
    print "recovered point is xyz = " + str(x) + "," + str(y) + "," + str(z)

    cam.plotAxes()

    #let's create an arbitrary road in front of a vehicle, with one yellow and one gray line.
