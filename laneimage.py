import cv2
from matplotlib.pyplot import *
from camera.camera import Camera
from numpy import *
c = Camera()

def laneImage(cam=c,height=480,width=640,xpts=arange(0,50,.1),ypts=zeros(len(arange(0,50,.1))),zpts=zeros(len(arange(0,50,.1))),linethickness=3*.0254,yellow_gap=7*.0254,lane_width=4.0):
	''' This creates a numpy array image (cv2) with a green ground, blue sky, and a road.
		The road is defined (currently) by a double yellow on the left and a single white on the right.
		laneImage(camera,xpts,ypts,zpts,linethickness,yellow_gap,lane_width):
		returns img
		You are to feed xpts,ypts,zpts as RELATIVE points-- this way, we avoid map dependencies and
		confusion with rotations, etc. Do this BEFORE you render. All we do here is project your points
		on to a simulated image. 
	'''
	#set the colors for the road, and lines.
	yellow_color = [255,255,0]
	white_color = [255,255,255]
	road_color = [128,128,128]
	#initialize image with green grass and blue sky 
	img = zeros((height,width,3), uint8)
	img[height/2:,:,1]=160
	img[height/2:,:,0]=128
	img[height/2:,:,2]=128
	img[0:height/2,:,2]=255
	img[0:height/2,:,1]=128
	img[0:height/2,:,0]=128
	#iterate through points to get row, column of each.
	imcoords_leftleft = None
	imcoords_left = None
	imcoords_right = None
	#loop through our world points
	for ind in range(0,len(xpts)):
		#calculate the world points for each line
		worldpt_leftleftline = array([xpts[ind],ypts[ind]+lane_width/2+yellow_gap,zpts[ind]])
		worldpt_leftline = array([xpts[ind],ypts[ind]+lane_width/2,zpts[ind]])
		worldpt_rightline = array([xpts[ind],ypts[ind]-lane_width/2,zpts[ind]])
		#print worldpt_leftleftline,worldpt_leftline,worldpt_rightline
		
		#calculate the row, col for each line at this distance ahead
		u_ll,v_ll,w_ll = cam.projectToImageRowCol(worldpt_leftleftline)
		#print cam.projectToImageCentered(array([xpts[ind],lane_width,zpts[ind]]))
		linethick_i,junk=cam.projectToImageCentered(array([xpts[ind],linethickness,zpts[ind]]))
		#if point is on image, append.
		if (u_ll/w_ll<width and v_ll/w_ll<height and u_ll/w_ll>0 and v_ll/w_ll>0):
			#print "hey"
			if imcoords_leftleft==None:
				imcoords_leftleft=array([int(u_ll/w_ll),int(v_ll/w_ll),int(abs(linethick_i))])
			else:
				imcoords_leftleft=vstack((imcoords_leftleft,array([int(u_ll/w_ll),int(v_ll/w_ll),int(abs(linethick_i))])))
		

		#calculate the row, col for each line at this distance ahead
		u_l,v_l,w_l = cam.projectToImageRowCol(worldpt_leftline)
		linethick_i,junk=cam.projectToImageCentered(array([xpts[ind],linethickness,zpts[ind]]))
		#if point is on image, append.
		if (u_l/w_l<width and v_l/w_l<height and u_l/w_l>0 and v_l/w_l>0):
			
			if imcoords_left==None:
				#print "doin it"
				imcoords_left=array([int(u_l/w_l),int(v_l/w_l),int(abs(linethick_i))])
			else:
				imcoords_left=vstack((imcoords_left,array([int(u_l/w_l),int(v_l/w_l),int(abs(linethick_i))])))
				#print imcoords_left
			#calculate the row, col for each line at this distance ahead
		u_r,v_r,w_r = cam.projectToImageRowCol(worldpt_rightline)
		linethick_i,junk=cam.projectToImageCentered(array([xpts[ind],linethickness,zpts[ind]]))		#if point is on image, append.
		if (u_r/w_r<width and v_r/w_r<height and u_r/w_r>0 and v_r/w_r>0):
			#print array([int(u_r/w_r),int(v_r/w_r),int(abs(linethick_i))])
			if imcoords_right==None:
				imcoords_right=array([int(u_r/w_r),int(v_r/w_r),int(abs(linethick_i))])
			else:
				imcoords_right=vstack((imcoords_right,array([int(u_r/w_r),int(v_r/w_r),int(abs(linethick_i))])))
		#print u_ll/w_ll,u_l/w_l,u_r/w_r
	#now, we want to fill in the road surface grey. We will do this using fillpoly.
	from numpy import int32 as npint
	#print npint(imcoords_left[:,0:-1])
	#print imcoords_right[:,1],imcoords_left[:,1]
	#NOW fill in the grey road surface.
	cv2.fillPoly(img, [npint(vstack((imcoords_right[:,0:-1],flipud(imcoords_leftleft[:,0:-1]))))],road_color)
	#NOW fill in the remaining road pieces
	ptsss = int32(array([[imcoords_leftleft[0,0],imcoords_leftleft[0,1]],[imcoords_right[0,0],imcoords_right[0,1]],[imcoords_right[0,0],height],[imcoords_leftleft[0,0],height]]))
	print ptsss
	cv2.fillConvexPoly(img,ptsss,road_color)
	#NOW loop through the im coords for each line and draw the lines with the correct thicknesses.
	for ind in range(1,len(imcoords_leftleft)):
		#get the points of our trapezoid.
		#print imcoords_leftleft[ind]
		x1 = int(imcoords_leftleft[ind-1,0]+imcoords_leftleft[ind-1,2]/2.0)
		x2 = int(imcoords_leftleft[ind-1,0]-imcoords_leftleft[ind-1,2]/2.0)
		x3 = int(imcoords_leftleft[ind,0]+imcoords_leftleft[ind,2]/2.0)
		x4 = int(imcoords_leftleft[ind,0]-imcoords_leftleft[ind,2]/2.0)
		y1 = int(imcoords_leftleft[ind-1,1])
		y2=y1
		y3 = int(imcoords_leftleft[ind,1])
		y4=y3
		points = int32(array([[x1,y1],[x2,y2],[x4,y4],[x3,y3]]))
		#print points
		cv2.fillConvexPoly(img, points, yellow_color)
	for ind in range(1,len(imcoords_left)):
		#get the points of our trapezoid.
		#print imcoords_left[ind]
		x1 = int(imcoords_left[ind-1,0]+imcoords_left[ind-1,2]/2.0)
		x2 = int(imcoords_left[ind-1,0]-imcoords_left[ind-1,2]/2.0)
		x3 = int(imcoords_left[ind,0]+imcoords_left[ind,2]/2.0)
		x4 = int(imcoords_left[ind,0]-imcoords_left[ind,2]/2.0)
		y1 = int(imcoords_left[ind-1,1])
		y2=y1
		y3 = int(imcoords_left[ind,1])
		y4=y3
		points = int32(array([[x1,y1],[x2,y2],[x4,y4],[x3,y3]]))
		#print points
		cv2.fillConvexPoly(img, points, yellow_color)
	for ind in range(1,len(imcoords_right)):
		#get the points of our trapezoid.
		#print imcoords_right[ind]
		x1 = int(imcoords_right[ind-1,0]+imcoords_right[ind-1,2]/2.0)
		x2 = int(imcoords_right[ind-1,0]-imcoords_right[ind-1,2]/2.0)
		x3 = int(imcoords_right[ind,0]+imcoords_right[ind,2]/2.0)
		x4 = int(imcoords_right[ind,0]-imcoords_right[ind,2]/2.0)
		y1 = int(imcoords_right[ind-1,1])
		y2=y1
		y3 = int(imcoords_right[ind,1])
		y4=y3
		points = int32(array([[x1,y1],[x2,y2],[x4,y4],[x3,y3]]))
		#print points
		cv2.fillConvexPoly(img, points, white_color)
	#cv2.polylines(img, [int32(imcoords_leftleft[:,0:-1])],0,yellow_color)
	#cv2.polylines(img, [int32(imcoords_left[:,0:-1])],0,yellow_color)
	#cv2.polylines(img, [int32(imcoords_right[:,0:-1])],0,white_color)
	
	return img


if __name__ == '__main__':
	""" this is a demo for the simple lane renderer module.  """
	print "You have chosen to run the demo."
	figure()
	img = laneImage()
	imshow(img)
	show()
	


